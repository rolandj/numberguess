# NumberGuess, number guessing game 

![Live](https://imgur.com/C7QyU3S.gif)

### 1. How it works
Client connects to server and tries to guess random number.

### 2. Libraries used
Python sockets. Connection is established via UDP.