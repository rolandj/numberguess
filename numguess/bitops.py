def encode(bit_string) -> bytes:
    """Transform human-readable string of bits to python bytes data."""
    bit_string = bit_string.zfill(16)
    bits = int(bit_string, 2)
    b = bytearray()
    while bits:
        b.append(bits & 0xff)
        bits >>= 8
    return bytes(b[::-1])


def decode(bite_data) -> str:
    """Transform bytes object to human-readable string of bits. """
    bits = bite_data.hex()
    bits = bin(int(bits, base=16))
    bits = str(bits[2:])
    bits = bits.zfill(16)
    return bits


def make_bin(decimal, fill) -> str:
    """Transform decimal to string of bits. """
    decimal = "{0:b}".format(decimal)
    decimal = decimal.zfill(fill)
    return decimal
