#!/usr/bin/env python3

import socket
from bitops import decode, encode, make_bin


class Client:
    REQUEST_INIT = '11110000000{}'
    GUESSED = '111'
    MISSED = '001'
    INIT = '100'
    LOOSED = '000'

    def __init__(self, host='127.0.0.1', port=5000, buffer=1024):
        """Initialize client with given parameters."""
        self.HOST = host
        self.PORT = port
        self.BUFFER = buffer
        self.main_socket = socket.socket(socket.AF_INET, type=socket.SOCK_DGRAM)
        self.started = False
        self.session = self.attempts = 0

    def read_number(self, range):
        """Get number from input."""
        number = -1
        while number not in range:
            try:
                if not self.started:
                    number = int(input(
                        "Enter how many approaches you wish to have, "
                        "(even number, max 10): "))
                else:
                    number = int(input("Enter  number in range [0;31]: "))
                print(f'User entered number: {number}')
            except ValueError:
                print("Wrong value, please try again.")
            if not self.started and number % 2 == 1:
                print("Please enter even number, try again.")
                number = -1

        return number

    def run(self):

        with self.main_socket:
            # Define destination address.
            self.main_socket.connect((self.HOST, self.PORT))

            while True:
                if not self.started:
                    # Send init game request and negotiate maximum attempts.
                    num = self.read_number(range(11))
                    bin_num = make_bin(num, 5)
                    message = Client.REQUEST_INIT.format(bin_num)
                    self.main_socket.send(encode(message))
                    self.started = True

                # Receive and decode data.
                received, host_ip = self.main_socket.recvfrom(1024)
                received = decode(received)
                response = received[4:7]

                if response == Client.INIT:
                    # Interpret data from server.
                    self.attempts = int(received[11:], base=2)
                    print(f'Client has negotiated {self.attempts} attempts.')
                    self.session = int(received[7:11], 2)
                    print(f'Session id is: {self.session}')
                    # Start guessing.
                    number = self.read_number(range(32))
                    number = make_bin(number, fill=5)
                    # Format message and send.
                    message = received[:4] + '000' + received[7:11] + number
                    self.main_socket.send(encode(message))

                elif response == Client.MISSED:
                    print(f'Session id is: {self.session}')
                    self.attempts -= 1
                    print(f"Missed, try again! {self.attempts} attempts left.")
                    # If missed, keep guessing.
                    number = self.read_number(range(32))
                    number = make_bin(number, fill=5)
                    # Format message and send.
                    message = '0011000' + received[7:11] + number
                    self.main_socket.send(encode(message))

                elif response == Client.GUESSED:
                    x = int(received[11:], 2)
                    print(f'Win! You guessed the number after {x} times.')
                    break

                elif response == Client.LOOSED:
                    x = int(received[11:], base=2)
                    print(f'Lost. Total approaches:  {x}.')
                    break

                else:
                    print('Wrong response: ', response)
                    break


if __name__ == '__main__':
    client = Client()
    client.run()
