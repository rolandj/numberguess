#!/usr/bin/env python3

import socket
import random
from collections import namedtuple
from bitops import decode, encode, make_bin


class ClientList:
    """Defines collection of client objects.

    Provides information about specific client for server.
    Client is represented by namedtuple with:
        addr: tuple, ip address and port number,
        id: particular client's session unique identifier,
        secret_num: client's secret number to guess,
        max: maximum approaches,
        counter: number of performed attempts.
    """

    ClientModel = namedtuple('ClientModel',
                             ['addr', 'id', 'secret_num', 'max', 'counter'])

    def __init__(self):
        self._list = []

    def __getitem__(self, item):
        for cl in self._list:
            if cl.addr == item:
                return cl
        else:
            raise NameError('Client not defined')

    def is_unique(self, token):
        """Check if given session id value is free."""
        for cl in self._list:
            if token == cl.id:
                return False
        return True

    def add(self, addr, token, num, limit):
        new_client = ClientList.ClientModel(addr, token, num, limit, [])
        new_client.counter.append(0)
        self._list.append(new_client)

    def remove(self, addr):
        for cl in self._list:
            if addr == cl.addr:
                self._list.remove(cl)
                break
        else:
            raise NameError('Client not defined')


class Server:
    CLIENT_STARTS = '1111000'
    CLIENT_PLAYS = '0011000'
    CLIENT_GUESSED = '0011111{}{}'
    CLIENT_MISSED = '0011001{}00000'
    START = '0011100{}{}'
    END = '0000000{}{}'

    def __init__(self, ip='127.0.0.1', port=5000, buffer=16, players=2):
        """Initialize server with given parameters."""
        self.HOST = ip
        self.PORT = port
        self.BUFFER = buffer
        self.server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.clients = ClientList()
        self.games_to_play = players

    def generate_id(self):
        """Generate unique session identifier (even number)."""
        unique, session_id = False, None
        while not unique:
            session_id = random.randint(1, 14)
            if session_id % 2 == 0:
                session_id = make_bin(session_id, 4)
                unique = self.clients.is_unique(session_id)
        return session_id

    @staticmethod
    def generate_secret_number():
        return make_bin(random.randint(0, 31), 5)

    def run(self):
        self.server.bind((self.HOST, self.PORT))

        with self.server:
            print(f'Server up on {self.HOST}:{self.PORT}.')

            while self.games_to_play:

                # Receive, decode and interpret data.
                received, address = self.server.recvfrom(self.BUFFER)
                data = decode(received)
                request, guess = data[:7], data[11:]

                Server.log(data, address, False)

                if request == Server.CLIENT_STARTS:
                    session = self.generate_id()
                    # Calculate player's maximum approaches.
                    bin_session = int(session, base=2)
                    clients_will = int(guess, base=2)
                    attempts = (clients_will + bin_session) // 2
                    bin_attempts = make_bin(attempts, 5)
                    # Generate secret number and save new player.
                    number = Server.generate_secret_number()
                    self.clients.add(address, session, number, attempts)
                    # Contact client.
                    message = Server.START.format(session, bin_attempts)
                    self.server.sendto(encode(message), address)
                    Server.log(message, address, True, session, number, True)

                elif request == Server.CLIENT_PLAYS:
                    player = self.clients[address]
                    self.play(player, guess)

                else:
                    print('Wrong request: ', request)
                    break

    def play(self, player, number):
        """Carry out game with specific player.

        As player model is tuple, it must store mutable attempt counter in first
        element of list - "player.counter[0]"."""
        player.counter[0] += 1
        attempts = player.counter[0]
        bin_attempts = make_bin(attempts, 5)

        if number == player.secret_num and attempts <= player.max:
            message = Server.CLIENT_GUESSED.format(player.id, bin_attempts)
            Server.log(message, player.addr, True)
            self.server.sendto(encode(message), player.addr)
            self.clients.remove(player.addr)
            self.games_to_play -= 1
            Server.log(0, player.addr, attempts)

        elif number != player.secret_num and attempts >= player.max:
            message = Server.END.format(player.id, bin_attempts)
            Server.log(0, player.addr, False)
            self.server.sendto(encode(message), player.addr)
            self.clients.remove(player.addr)
            self.games_to_play -= 1

        else:
            message = Server.CLIENT_MISSED.format(player.id)
            Server.log(message, player.addr, True)
            self.server.sendto(encode(message), player.addr)

    @staticmethod
    def log(data, address, send, token='', num='', init=False):
        """Log specific server event.

        
        """
        client = f'{address[0]}:{address[1]}'
        if init:
            print(f'New game with: {client}, id: {token}, secret_number: {num}')
        elif send and data:
            print(f'Sending: {data} to: {client}')
        elif not data and send:
            print(f'Player {client} guessed the number with {send} approaches')
        elif not data and not send:
            print(f'Player {client} out of approaches.')
        else:
            print(f'Received {data} from {client}')


if __name__ == '__main__':
    server = Server()
    server.run()
